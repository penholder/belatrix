require('whatwg-fetch')
//
const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const globImporter = require('node-sass-glob-importer')
const HtmlWebpackPlugin = require('html-webpack-plugin')
//
module.exports = (env, options) => {
	console.log('\x1b[36m%s\x1b[0m', `>>>>>>> Webpack mode: ${options.mode}`)
	return {
		entry: ['whatwg-fetch', './src/index.js'],
		output: {
			path: path.join(__dirname, '/public'),
			filename: 'js/stark.bundle.js',
			publicPath: '/'
		},
		module: {
			rules: [
				{
					test: /\.js$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader'
					}
				},
				{
					test: /\.(s?css)$/,
					use: [
						{
							loader: 'css-hot-loader',
							options: {}
						},
						{
							loader: MiniCssExtractPlugin.loader,
							options: {}
						},
						{
							loader: 'css-loader',
							options: {}
						},
						{
							loader: 'postcss-loader',
							options: {
								plugins: () => [require('autoprefixer')]
							}
						},
						{
							loader: 'sass-loader',
							options: {
								importer: globImporter()
							}
						}
					]
				},
				{
					test: /\.(png|jpe?g|gif)$/,
					use: [
						{
							loader: 'file-loader',
							options: {
								name:
									options.mode === 'production'
										? '[name].[hash:12].[ext]'
										: '[name].[ext]',
								outputPath: 'images'
							}
						}
					]
				},
				{
					test: /\.(eot|svg|ttf|woff|woff2)$/,
					use: [
						{
							loader: 'url-loader',
							options: {
								name: '[name].[ext]',
								outputPath: 'css/fonts'
							}
						}
					]
				}
			]
		},
		devServer: {
			historyApiFallback: true
		},
		plugins: [
			new HtmlWebpackPlugin({
				template: path.join(__dirname, '/src/index.html')
			}),
			new MiniCssExtractPlugin({
				filename: 'css/style.stark.css'
			})
			// new FaviconsWebpackPlugin({
			// 	logo: './src/images/favicon.png',
			// 	background: '#3f4458',
			// 	icons: {
			// 		android: true,
			// 		appleIcon: true,
			// 		appleStartup: false,
			// 		coast: false,
			// 		favicons: true,
			// 		firefox: false,
			// 		opengraph: false,
			// 		twitter: false,
			// 		yandex: false,
			// 		windows: true
			// 	}
			// })
		]
	}
}
