import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Provider } from './ctx/context'
// components
import ScrollToTop from './components/scroll-top'
import IndexPage from './components/pages/index-page'
import TravelPage from './components/pages/travel-page'
import TravelForm from './components/travel-form'
import Header from './components/header'

class App extends Component {
	render() {
		return (
			<Provider>
				<BrowserRouter>
					<ScrollToTop>
						<Header />
						<Switch>
							<Route exact path="/" component={IndexPage} />
							<Route exact path="/travel" component={TravelPage} />
							<Route path="/travel/book-trip" component={TravelForm} />
							<Route render={() => <div>404</div>} />
						</Switch>
					</ScrollToTop>
				</BrowserRouter>
			</Provider>
		)
	}
}

export default App
