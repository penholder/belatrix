import React, { Component } from 'react'
import firebase from '../services/firebase'

// copia local de datos
//import dummy from './dummy/passengers'

const Context = React.createContext()
const db = firebase.firestore()

export class Provider extends Component {
	constructor() {
		super()
		this.state = {
			passengersList: [],
			// precios que deberian venir de una base de datos
			prices: {
				trip: 10000,
				child: 5000,
				toddlers: 0,
				luggage: 500,
				halfService: 300,
				fullService: 600,
				drinks: 150,
				entertainment: 150
			},
			addPassenger: this.addPassenger
		}
	}

	addPassenger = passenger => {
		const { passengersList } = this.state
		passengersList.push(passenger)
		this.setState({ passengersList })

		// añade el objeto (passenger) a la base de datos de firebase
		db.collection('Passengers')
			.add({
				...passenger
			})
			.then(docRef => console.log('New passenger ID: ', docRef.id))
			.catch(error => console.error('Error adding document: ', error))
	}

	// trae la lista de pasajeros de la base de datos
	getPassengers = () => {
		const { passengersList } = this.state
		const app = this

		db.collection('Passengers')
			.get()
			.then(querySnapshot => {
				querySnapshot.forEach(doc => passengersList.push({ id: doc.id, ...doc.data() }))
				app.setState({ passengersList })
			})
	}

	componentDidMount() {
		this.getPassengers()
	}

	render() {
		return <Context.Provider value={this.state}>{this.props.children}</Context.Provider>
	}
}

export const Consumer = Context.Consumer
