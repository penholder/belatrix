import firebase from 'firebase/app'
// import 'firebase/auth'
import 'firebase/firestore'

const config = {
	apiKey: 'AIzaSyDg9hppDEZMvReKBqStXjeCDaMO0zZqYyU',
	authDomain: 'starkspacetravel.firebaseapp.com',
	databaseURL: 'https://starkspacetravel.firebaseio.com',
	projectId: 'starkspacetravel',
	storageBucket: 'starkspacetravel.appspot.com',
	messagingSenderId: '289163166559'
}

firebase.initializeApp(config)
//firebase.firestore()

export default firebase
