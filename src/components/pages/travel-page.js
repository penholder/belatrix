import React from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'

import { Consumer } from '../../ctx/context'
import ProgressRing from '../progress-ring'

const TravelPage = () => {
	const maxPassengers = 20 // este numero es arbitrario pero deberia venir de la db
	const minPassengers = Math.floor(maxPassengers * 0.8)
	const today = String(new Date().getDate()).padStart(2, '0')

	return (
		<Consumer>
			{({ passengersList }) => {
				if (passengersList.length > 0) {
					const dates = passengersList
						.map(({ departureDate, returnDate }) => {
							// se crean arrays independiantes de cada fecha de salida y llegada
							const departureDay = departureDate.split('-').map(i => parseInt(i))
							const returnDay = returnDate.split('-').map(i => parseInt(i))

							return { departureDay, returnDay }
						})
						.reduce((grouped, date) => {
							// se agrupan en dos arrays independientes
							Object.keys(date).forEach(key => {
								grouped[key] = grouped[key] || []
								grouped[key].push({
									date: date[key]
								})
							})

							return grouped
						}, {})

					const [departuresCount, returnCount] = Object.keys(dates).map(key => {
						const d = Object.values(
							dates[key].reduce((agr, { date }) => {
								// se agrupan las fechas iguales y se cuentan los pasajeros
								if (date) {
									agr[date] = agr[date] || { date, passengers: 0 }
									agr[date].passengers++
								}
								return agr
							}, {})
						)
						return d
					})

					/*
						se filtran las fechas disponibles teniendo en cuenta si el cupo de pasajeros esta completo y que haya una antelacion de 48hs.
					*/
					const availables = departuresCount.filter(
						count => count.passengers < maxPassengers && count.date[0] > parseInt(today) + 2
					)

					// se busca la fecha más cercana disponible
					const next = availables.find(trip => {
						const min = Math.min(...availables.map(count => count.date[0]))
						return trip.date[0] === min
					})

					// formato de la fecha a mostrar
					const nextDate = moment(
						new Date(next.date[2], next.date[1] - 1, next.date[0])
					).format('MMMM Do, YYYY')

					return (
						<section className="travel">
							<div className="travel-box">
								<div className="data">
									<div className="travel-data">
										<h3>Our next available travel:</h3>
										<h4 className="date">{nextDate}</h4>
										<div className="content">
											<p>We fly every Saturday and come back to Earth on Sundays.</p>
											<p>The minimun stay is one week.</p>
											<p>
												<small>Trip should be booked 48hs before.</small>
											</p>
										</div>
									</div>
									<div className="passengers-data">
										<div className="content">
											<h4>Passengers</h4>
											<p>
												<span>At 80% ({minPassengers}p)</span>
												<br />
												<span>we are ready to launch.</span>
												<br />
												<span>Hurry!</span>
											</p>
										</div>
										<div className="ring-wrapper">
											<div className="count">
												<span>{next.passengers}/20</span>
											</div>
											<ProgressRing
												className="passengers-count"
												radius={80}
												stroke={8}
												progress={(next.passengers * 100) / maxPassengers}
												delay={600}
											/>
										</div>
									</div>
								</div>
								<div className="cta-wrapper">
									<div className="content">
										<p>
											<strong>Book Now!</strong>
											<br />
											<span>from</span>
										</p>
										<p className="price">
											<strong>U$D 10 000</strong>
										</p>
									</div>
									<Link
										className="cta white-pill"
										to={{
											pathname: '/travel/book-trip',
											state: {
												availables
											}
										}}
									>
										Book
									</Link>
								</div>
							</div>
						</section>
					)
				} else {
					return (
						<div className="travel-form">
							<h1>travel</h1>
							<span>loading...</span>
						</div>
					)
				}
			}}
		</Consumer>
	)
}

export default TravelPage
