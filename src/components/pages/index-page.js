import React from 'react'
import { Link } from 'react-router-dom'

import hero from '../../images/moon_bg_01.jpg'

const IndexPage = () => (
	<section className="hero">
		<div className="hero-img">
			<img src={hero} alt="[ moon image ]" />
		</div>
		<div className="hero-content">
			<h3 className="tag">To the Moon and Back</h3>
			<h4>The next great interplanetary travel service</h4>
			<p>Easily book unique space trips and experience the universe in style</p>
			<Link className="cta" to="/travel">
				Travel Now
			</Link>
		</div>
	</section>
)

export default IndexPage
