import React from 'react'
import { Consumer } from '../ctx/context'
import moment from 'moment'
import Flatpickr from 'react-flatpickr'

import 'flatpickr/dist/themes/dark.css'

class TravelForm extends React.Component {
	constructor() {
		super()
		this.state = {
			firstName: '',
			lastName: '',
			phone: '',
			email: '',
			departureDate: '',
			returnDate: '',
			adults: 1,
			children: 0,
			toddlers: 0,
			luggage: 0,
			halfService: false,
			fullService: false,
			drinks: false,
			entertainment: false,
			invalid: '',
			valid: false
		}
	}

	handleOnChange = e => {
		const {
			currentTarget: { name, value }
		} = e

		this.setState({ [name]: value })
	}

	handleCount = (name, value) => {
		// se asugura que el valor no sea negativo
		const val = this.state[name] + value < 0 ? 0 : this.state[name] + value
		this.setState({ [name]: val })
	}

	handleSubmit = (e, addPassenger) => {
		e.preventDefault()
		const { firstName, lastName, phone, email, departureDate, returnDate } = this.state
		const passenger = {
			firstName,
			lastName,
			phone,
			email,
			departureDate,
			returnDate
		}

		// validaciones
		const valid = !Object.entries(passenger).some(entry => {
			const phoneRegEx = /^[0-9]*$/
			const emailRegEx = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/
			const invalid =
				entry[1] === '' ||
				(entry[0] === 'email' && !emailRegEx.test(entry[1])) ||
				(entry[0] === 'phone' && !phoneRegEx.test(entry[1]))
			invalid && this.setState({ invalid: entry[0] })

			return invalid
		})

		if (valid) {
			// reset state exept valid prop
			this.setState({
				firstName: '',
				lastName: '',
				phone: '',
				email: '',
				departureDate: '',
				returnDate: '',
				adults: 1,
				children: 0,
				toddlers: 0,
				luggage: 0,
				halfService: false,
				fullService: false,
				drinks: false,
				entertainment: false,
				invalid: '',
				valid
			})
			// se añade el pasajero a la db
			addPassenger({
				firstName,
				lastName,
				phone,
				email,
				departureDate,
				returnDate
			})
		}
	}

	render() {
		return (
			<Consumer>
				{({ prices, addPassenger }) => {
					if (prices && addPassenger) {
						const { availables = [] } = this.props.location.state
						// array de fechas disponibles para el selector del formulario
						const selectables = availables.map(trip => {
							const [day, month, year] = trip.date
							return moment(new Date(year, month - 1, day)).format('DD-MM-YYYY')
						})

						const {
							adults,
							children,
							toddlers,
							luggage,
							halfService,
							fullService,
							drinks,
							entertainment
						} = this.state

						// calculo en tiempo real del valor de pasaje
						let finalPrice = prices.trip * parseInt(adults)
						finalPrice += prices.child * parseInt(children)
						finalPrice += prices.toddlers * parseInt(toddlers)
						finalPrice += prices.luggage * luggage
						finalPrice += halfService
							? prices.halfService * (parseInt(adults) + parseInt(children))
							: 0
						finalPrice += fullService
							? prices.fullService * (parseInt(adults) + parseInt(children))
							: 0
						finalPrice += drinks ? prices.drinks : 0
						finalPrice += entertainment ? prices.entertainment : 0

						return (
							<section className="form-page">
								<div className="form-block">
									<h2 className="form-title">Book your trip</h2>
									<form className="trip-form">
										<div className="passengers-info">
											<div
												className={
													this.state.invalid == 'firstName'
														? 'form-field invalid'
														: 'form-field'
												}
											>
												<input
													className="input"
													type="text"
													name="firstName"
													id="firstName"
													value={this.state.firstName}
													required
													placeholder=" "
													onChange={this.handleOnChange}
												/>
												<label htmlFor="firstName">Name</label>
											</div>
											<div
												className={
													this.state.invalid == 'lastName'
														? 'form-field invalid'
														: 'form-field'
												}
											>
												<input
													className="input"
													type="text"
													name="lastName"
													id="lastName"
													value={this.state.lastName}
													required
													placeholder=" "
													onChange={this.handleOnChange}
												/>
												<label htmlFor="lastName">Last Name</label>
											</div>
											<div
												className={
													this.state.invalid == 'phone'
														? 'form-field invalid'
														: 'form-field'
												}
											>
												<input
													className="input"
													type="tel"
													name="phone"
													id="phone"
													value={this.state.phone}
													required
													placeholder=" "
													onChange={this.handleOnChange}
												/>
												<label htmlFor="phone">Phone</label>
											</div>
											<div
												className={
													this.state.invalid == 'email'
														? 'form-field invalid'
														: 'form-field'
												}
											>
												<input
													className="input"
													type="email"
													name="email"
													id="email"
													value={this.state.email}
													required
													placeholder=" "
													onChange={this.handleOnChange}
												/>
												<label htmlFor="email">Email</label>
											</div>
											<div
												className={
													this.state.invalid == 'departureDate'
														? 'form-field invalid'
														: 'form-field'
												}
											>
												<Flatpickr
													className="input"
													name="departureDate"
													placeholder="Departure"
													value={this.state.departureDate}
													options={{ dateFormat: 'd-m-Y', enable: selectables }}
													onChange={(selected, dateStr) =>
														this.handleOnChange({
															currentTarget: {
																name: 'departureDate',
																value: dateStr
															}
														})
													}
												/>
											</div>
											<div
												className={
													this.state.invalid == 'returnDate'
														? 'form-field invalid'
														: 'form-field'
												}
											>
												<Flatpickr
													className="input"
													name="returnDate"
													placeholder="Return"
													value={this.state.returnDate}
													options={{
														dateFormat: 'd-m-Y',
														enable: [
															date =>
																// solo domingos disponibles
																date.getDay() == 0 &&
																date.getDate() > new Date().getDate()
														]
													}}
													onChange={(selected, dateStr) =>
														this.handleOnChange({
															currentTarget: {
																name: 'returnDate',
																value: dateStr
															}
														})
													}
												/>
											</div>
											<div className="form-field options">
												<div className="counter">
													<span
														className="btn minus"
														onClick={() => this.handleCount('adults', -1)}
													>
														&ndash;
													</span>
													<input
														className="input"
														type="text"
														name="adults"
														id="adults"
														required
														placeholder=" "
														value={this.state.adults}
														onChange={({ currentTarget: { name } }) =>
															this.handleOnChange(name)
														}
													/>
													<span
														className="btn add"
														onClick={() => this.handleCount('adults', +1)}
													>
														+
													</span>
												</div>
												<label htmlFor="adults">Adults:</label>&nbsp;
												<span className="info-msg">
													<strong>U$D {prices.trip}</strong> each
												</span>
											</div>
											<div className="form-field options">
												<div className="counter">
													<span
														className="btn minus"
														onClick={() => this.handleCount('children', -1)}
													>
														&ndash;
													</span>
													<input
														className="input"
														type="text"
														name="children"
														id="children"
														required
														placeholder=" "
														value={this.state.children}
														onChange={({ currentTarget: { name } }) =>
															this.handleOnChange(name)
														}
													/>
													<span
														className="btn add"
														onClick={() => this.handleCount('children', +1)}
													>
														+
													</span>
												</div>
												<label htmlFor="children">Children:</label>&nbsp;
												<span className="info-msg">
													<strong>U$D {prices.child}</strong> each
												</span>
											</div>
											<div className="form-field options">
												<div className="counter">
													<span
														className="btn minus"
														onClick={() => this.handleCount('toddlers', -1)}
													>
														&ndash;
													</span>
													<input
														className="input"
														type="text"
														name="toddlers"
														id="toddlers"
														required
														placeholder=" "
														value={this.state.toddlers}
														onChange={({ currentTarget: { name } }) =>
															this.handleOnChange(name)
														}
													/>
													<span
														className="btn add"
														onClick={() => this.handleCount('toddlers', +1)}
													>
														+
													</span>
												</div>
												<label htmlFor="toddlers">Toddlers:</label>&nbsp;
												<span className="info-msg">
													{prices.toddlers > 0 ? (
														<React.Fragment>
															<strong>U$D {prices.toddlers}</strong> each
														</React.Fragment>
													) : (
														<strong>Free</strong>
													)}{' '}
													<span>(special seat needed)</span>
												</span>
											</div>
										</div>
										<div className="extras">
											<h3 className="extras-title">Extras</h3>
											<div className="form-field options">
												<div className="counter">
													<span
														className="btn minus"
														onClick={() => this.handleCount('luggage', -1)}
													>
														&ndash;
													</span>
													<input
														className="input"
														type="text"
														name="luggage"
														id="luggage"
														required
														placeholder=" "
														value={this.state.luggage}
														onChange={({ currentTarget: { name } }) =>
															this.handleOnChange(name)
														}
													/>
													<span
														className="btn add"
														onClick={() => this.handleCount('luggage', +1)}
													>
														+
													</span>
												</div>
												<label htmlFor="luggage" className="info-msg">
													Extra Luggage: <strong>+U$D {prices.luggage}</strong>
												</label>
											</div>
											<div className="form-field options with-check">
												<input
													className="check"
													type="checkbox"
													name="halfService"
													id="halfService"
													defaultChecked={this.state.halfService}
													onChange={() =>
														this.setState({ halfService: !this.state.halfService })
													}
												/>
												<label htmlFor="halfService">
													Half board service:{' '}
													<strong>+U$D {prices.halfService} /passenger</strong>
												</label>
											</div>
											<div className="form-field options with-check">
												<input
													className="check"
													type="checkbox"
													name="fullService"
													id="fullService"
													defaultChecked={this.state.fullService}
													onChange={() =>
														this.setState({ fullService: !this.state.fullService })
													}
												/>
												<label htmlFor="fullService">
													Full board service:{' '}
													<strong>+U$D {prices.fullService} /passenger</strong>
												</label>
											</div>
											<div className="form-field options with-check">
												<input
													className="check"
													type="checkbox"
													name="drinks"
													id="drinks"
													defaultChecked={this.state.drinks}
													onChange={() =>
														this.setState({ drinks: !this.state.drinks })
													}
												/>
												<label htmlFor="drinks">
													On board drinks service:{' '}
													<strong>+U$D {prices.drinks}</strong>
												</label>
											</div>
											<div className="form-field options with-check">
												<input
													className="check"
													type="checkbox"
													name="entertainment"
													id="entertainment"
													defaultChecked={this.state.entertainment}
													onChange={() =>
														this.setState({
															entertainment: !this.state.entertainment
														})
													}
												/>
												<label htmlFor="entertainment">
													Entertainment (movies, games V.R.):{' '}
													<strong>+U$D {prices.entertainment}</strong>
												</label>
											</div>
											<div className="price">
												<h4>Total:</h4>
												<p className="amount">U$D {finalPrice}</p>
											</div>
										</div>
										<input
											className="white-pill submit"
											type="submit"
											value="Book Trip"
											onClick={e => this.handleSubmit(e, addPassenger)}
										/>
									</form>
									<div
										className={
											this.state.valid ? 'confirm-modal visible' : 'confirm-modal'
										}
									>
										<h3>Thank you!</h3>
										<p>
											Your trip will be confirmed via email along with payment methods.
										</p>
										<button
											className="white-pill"
											onClick={() => this.setState({ valid: false })}
										>
											Book Another trip
										</button>
									</div>
								</div>
							</section>
						)
					} else {
						return (
							<section className="form-page">
								<div className="form-block">
									<span>loading...</span>
								</div>
							</section>
						)
					}
				}}
			</Consumer>
		)
	}
}

export default TravelForm
