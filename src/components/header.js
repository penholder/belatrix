import React from 'react'
import { NavLink } from 'react-router-dom'

import logo from '../images/stark_spacetravel-logo.svg'

const Header = () => (
	<header className="main-header">
		<h1 className="logo">
			<NavLink to={'/'}>
				<img src={logo} alt="Stark Spacetravel" />
			</NavLink>
		</h1>
		<nav className="main-nav">
			<NavLink className="nav-link" to={'/'} exact activeClassName="active">
				Home
			</NavLink>
			<NavLink className="nav-link" to={'/travel'} exact activeClassName="active">
				Travel
			</NavLink>
			<NavLink className="nav-link" to={'#'} exact activeClassName="active">
				About
			</NavLink>
			<NavLink className="nav-link" to={'#'} exact activeClassName="active">
				Contact
			</NavLink>
		</nav>
	</header>
)

export default Header
