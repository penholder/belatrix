import React from 'react'

class ProgressRing extends React.Component {
	constructor(props) {
		super(props)

		const { radius, stroke } = this.props

		this.state = { progress: 0 }
		this.normalizedRadius = radius - stroke * 2
		this.circumference = this.normalizedRadius * 2 * Math.PI
	}

	componentDidMount() {
		const { progress, delay = 500 } = this.props
		setTimeout(() => this.setState({ progress }), delay)
	}

	render() {
		const { radius, stroke, className } = this.props
		const { progress } = this.state

		const circleStyles = {
			strokeDashoffset: this.circumference - (progress / 100) * this.circumference,
			transition: 'stroke-dashoffset 0.5s',
			transform: 'rotate(-90deg)',
			transformOrigin: '50% 50%'
		}

		return (
			<svg className={className} height={radius * 2} width={radius * 2}>
				<circle
					data-progress={progress}
					stroke="white"
					fill="transparent"
					strokeWidth={stroke}
					strokeDasharray={this.circumference + ' ' + this.circumference}
					style={circleStyles}
					r={this.normalizedRadius}
					cx={radius}
					cy={radius}
				/>
			</svg>
		)
	}
}

export default ProgressRing
