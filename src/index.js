import React from 'react'
import ReactDOM from 'react-dom'

import App from './App'

import 'sanitize.css'
import './styles/index.scss'

ReactDOM.render(<App />, document.getElementById('main'))
