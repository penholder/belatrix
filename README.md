# Stark Spacetravel

Una nueva empresa de viajes a la Luna necesita hacer una página para que los pasajeros
puedan contratar sus servicios de forma online. Tu tarea consiste en crear una solución digital
que contemple lo siguiente:

1. Estos viajes salen los sábados y regresan los domingos, solo cuando la cantidad de
   pasajeros ha alcanzado por lo menos el 80% de su capacidad, por lo que el pasajero
   puede elegir una fecha tentativa pero solo se confirma el viaje cuando se ha alcanzado
   esa capacidad con un tiempo límite de 48 horas previas al viaje. También hay casos en
   que ese 80% ya está cubierto cuando el pasajero quiere registrarse, por lo que el viaje ya
   está confirmado en el momento del registro o incluso podría suceder que la
   confirmación del viaje dependa de los pasajes que está por reservar el usuario. La
   estadía mínima es de una semana y también estará sujeta a la cantidad de pasajeros
   para alcanzar el 80% de la capacidad de la nave que vuelve de la Luna a la Tierra, por lo
   tanto el usuario querrá saber ésta información con antelación para poder planear su
   viaje correctamente.

2. El costo del pasaje depende de los servicios que quiere incluir el pasajero.

   -  Si viaja sólo con equipaje de mano el costo del pasaje de un adulto es de **U\$S 10 000**
   -  Los niños pagan la mitad de la tarifa, mientras que los bebe viajan sin costo pero deben usar asientos especiales a pedido por normas de seguridad.
   -  Si quiere agregar una valija en bodega se sumaría **U\$S 500** por valija extra.
   -  El servicio a bordo de media pensión (desayuno y almuerzo) **U\$S 300 por pasajero**.
   -  La pensión completa (desayuno, almuerzo, merienda y cena) **U\$S 600 por pasajero**.
   -  El servicio de sólo bebidas tiene un costo de **U\$S150.**
   -  El uso de entretenimiento (películas, videojuegos y museo virtual) tiene un costo de **U\$S 100.**
